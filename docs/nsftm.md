####  Selection Detection From the Masses - a tale of bibliographic research, automation and best scientific practices

###### Computational Biology and Bioinformatics Seminar

</br>

<center>Francisco Pina Martins</center>


<p style="margin-bottom:0;"><small>[@FPinaMartins](https://twitter.com/FPinaMartins) - Twitter</small></p>
<p style="margin : 0; padding-top:0;"><small>[@FPinaMartins@scholar.social](https://scholar.social/@FPinaMartins) - Mastodon</small></p>

<img src="assets/cover_logos.png" style="background:none; border:none; box-shadow:none;" alt="FCUL, cE3c and CoBiG² logos" style="text-align: center" />

<p style="margin : 0; padding-top:0;"><small>February 2021</small></p>

---

### Evolution

<div class="fragment" style="float: right"><img src="assets/Darwin_TOL.jpg" alt="Darwin's Tree of life" style="background:none; border:none; box-shadow:none;"/></div>
<ul>
<li class="fragment">Mechanisms</li>
  <ul>
  <li class="fragment fade-in-then-semi-out">Mutation</li>
  <li class="fragment fade-in-then-semi-out">Gene-flow</li>
  <li class="fragment fade-in-then-semi-out">Genetic drift</li>
  <li class="fragment fade-in-then-semi-out">Non-random mating</li>
  <li class="fragment">[Natural] Selection</li>
  </ul>
</ul>

<div class="fragment" style="float: bottom-left"><img src="assets/find_robin.png" alt="Robin with magnifyng glass" style="background:none; border:none; box-shadow:none;"/></div>

---

### What is [Natural] Selection?

<ul>
<li class="fragment">A mechanism of evolution</li>
  <ul>
  <li class="fragment">There is variation in **phenotypes**</li>
  <li class="fragment">Phenotypes have **differential reproduction**</li>
  <li class="fragment">There is heritability</li>
  <li class="fragment">More **advantageous** phenotypes increase in frequency</li>
  </ul>
</ul>

|||

### What is [Natural] Selection?

<img src="assets/Selection.png" alt="Selection plot"  style="background:none; border:none; box-shadow:none;" />

---

### The importance of detecting selection

<ul>
<li class="fragment">Understand Evolutionary History</li>
<li class="fragment">Understand phenotypic diversity</li>
<li class="fragment">Understand adaptive traits' response</li>
  <ul>
  <li class="fragment">Ultimately required to understand how species can adapt to new conditions</li>
  </ul>
</ul>

<img src="assets/owls.jpg" class="fragment" style="background:none; border:none; box-shadow:none;" />

---

### Detecting Selection </br> (Model organism version)

<ul>
<li class="fragment">Whole genome (re)sequencing</li>
<li class="fragment">Can scan entire regions</li>
<li class="fragment">Laboratory breeding</li>
<li class="fragment">Controlled environments</li>
</ul>

|||

### Detecting Selection </br> ([Laurentino et al. 2020](https://doi.org/10.1038/s41467-020-15657-3))

<div class="fragment" style="float: left"><img src="assets/Laurentino_F1.png" alt="Figure 1 from Laurentino et al. 2020" style="background:none; border:none; box-shadow:none;"/></div>


<div class="fragment" style="float: left"><img src="assets/Laurentino_F2.png" alt="Figure 2 from Laurentino et al. 2020" style="background:none; border:none; box-shadow:none;"/></div>

<div class="fragment" style="float: right"><img src="assets/Laurentino_F3.png" alt="Figure 3 from Laurentino et al. 2020" style="background:none; border:none; box-shadow:none;"/></div>

<div class="fragment" style="float: right"><img src="assets/clock.png" alt="Wall clock" style="background:none; border:none; box-shadow:none;"/></div>

---

### Working with natural populations

<ul>
<li class="fragment">Non-model organisms</li>
<li class="fragment">No reference genome</li>
<li class="fragment">Large genomes</li>
<li class="fragment">Samples collected in the wild</li>
</ul>

<a href="https://commons.wikimedia.org/w/index.php?curid=3899733">
<div class="fragment" style="float: left"><img src="assets/Edzell_garden.jpg" alt="Walled Garden" style="background:none; box-shadow:none;"/></div>
</a>

<a href="https://commons.wikimedia.org/wiki/File:View_from_the_top,_Falealupo_Rainforest_canopy_walkway,_Savaii,_Samoa_2009.JPG#/media/File:View_from_the_top,_Falealupo_Rainforest_canopy_walkway,_Savaii,_Samoa_2009.JPG">
<div class="fragment" style="float: right"><img src="assets/wild.jpg" alt="Wild landscape" style="background:none; box-shadow:none;"/></div>
</a>

---

### Reduced Representation Libraries

<ul>
<li class="fragment">Genome fingerprinting techniques</li>
  <ul>
    <li class="fragment">Genotype pseudo-random genome locations</li>
  </ul>
<li class="fragment">Cheap to use for large numbers of individuals</li>
<li class="fragment">Adjusted to genome size</li>
</ul>

<p class="fragment">[RAD-Seq](https://doi.org/10.1371/journal.pone.0003376) & [GBS](https://doi.org/10.1371/journal.pone.0019379) (and variants)</p>

<div class="fragment" style="float: center"><img src="assets/genome_cutters.png" alt="DNA being cut by scissors" style="background:none; border:none; box-shadow:none;"/></div>

|||

### The concept

<img src="assets/RAD.png" style="background:none; border:none; box-shadow:none;">

|||

### The concept </br> (dry lab)

<img src="assets/ipyrad.png" style="background:none; border:none; box-shadow:none;">

---

### Detecting selection </br> (Non-models)
<style>
.container{
display: flex;
}
.col{
flex: 1;
}
</style>
<div class="container">
<div class="col">
<div class="fragment">
Outlier detection
<img src="assets/KLDs.png" alt="KLD Manhattan plot" style="background:none; border:none; box-shadow:none;" />
</br>
Find alleles with "unexpected" frequencies
<ul>
<li class="fragment">Bayescan</li>
<li class="fragment">SelEstim</li>
</ul>
</div>
</div>

<div class="col">
<div class="fragment">
Association with variable

<img src="assets/Association.png" alt="Correlation plot" style="background:none; border:none; box-shadow:none;" />
</br>
Find alleles that vary with external variables
<ul>
<li class="fragment">Baypass</li>
<li class="fragment">LFMM</li>
</ul>
</div>

</div>

---

### Current methods limitations

<ul>
<li class="fragment" data-fragment-index="1">[Number of false positives](https://onlinelibrary.wiley.com/doi/full/10.1111/mec.14584)</li>
<div class="fragment" data-fragment-index="1" style="float: right"><img src="assets/type-i-error.jpg" alt="You're preagnant" style="background:none; border:none; box-shadow:none;"/></div>
<li class="fragment" data-fragment-index="2">[Single locus](https://onlinelibrary.wiley.com/doi/full/10.1111/mec.14584)</li>
<div class="fragment" data-fragment-index="2" style="float: right"><img src="assets/Hexlet_800.gif" alt="Spheres" style="background:none; border:none; box-shadow:none;"/></div>
<li class="fragment" data-fragment-index="3">[CORRELATION DOES NOT MEAN CAUSATION!](https://www.tylervigen.com/spurious-correlations)</li>
<div class="fragment" data-fragment-index="3" style="float: left"><img src="assets/spurious.png" alt="Spurious correlation plot" style="background:none; border:none; box-shadow:none;"/></div>
</ul>

---

### Methods in wide usage

<ul>
<li class="fragment">Thousands of papers rely on these methods</li>
  <ul>
  <li class="fragment">[~~1721~~ 2036 citations for Bayescan alone](https://scholar.google.pt/scholar?cites=4319645506146909873&as_sdt=2005&sciodt=0,5&hl=pt-PT)</li>
  </ul>
<li class="fragment">This led to some interesting meta-analyses</li>
  <ul>
  <li class="fragment">[Ahrens et al. 2018](https://onlinelibrary.wiley.com/doi/full/10.1111/mec.14549)</li>
  </ul>
</ul>

<img src="assets/ahrens-01.png" class="fragment" style="background:none; border:none; box-shadow:none;" />

---

### The problem thus far...

<ul>
<li class="fragment">Lack of standard method</li>
<li class="fragment">Direct comparisons not possible</li>
  <ul>
  <li class="fragment">Within the same method</li>
  <li class="fragment">Between different methods</li>
  <li class="fragment">No baseline</li>
  </ul>
<li class="fragment">A lot of (good) work is being done, but it's hard to combine all the generated information!</li>
</ul>

<img src="assets/incomparable.png" class="fragment" style="background:none; border:none; box-shadow:none;" />

---

### Goals:

<ul>
<li class="fragment">Understand how much results depend on methods</li>
  <ul>
  <li class="fragment">Analysing multiple datasets with the same method</li>
  <li class="fragment">Analysing the same dataset with multiple methods</li>
  </ul>
<li class="fragment">Automating parameter estimation</li>
  <ul>
  <li class="fragment">No "one size fits all", but rules are automatable</li>
  </ul>
<li class="fragment">Effectively define a baseline</li>
</ul>

<img src="assets/orpple.png" class="fragment" style="background:none; border:none; box-shadow:none;" alt="Apple and orange 'hybrid'" />

---

### How to achieve them? (I)

<ul>
<li class="fragment">Re-analyzing datasets from the literature</li>
<li class="fragment">Requirements:</li>
  <ul>
  <li class="fragment">SNP data available (VCF)</li>
  <li class="fragment">Sample coordinates available</li>
  </ul>
</ul>

<img src="assets/Minions.png" class="fragment" alt="My minions" style="float: center; background:none; border:none; box-shadow:none;" />

---

### How to achieve them? (II)

<ul>
<li class="fragment">Standardization</li>
  <ul>
  <li class="fragment">All datasets are analysed in the same way</li>
  </ul>
<li class="fragment">Automation</li>
  <ul>
  <li class="fragment">All analyses performed with a single command</li>
  </ul>
<li class="fragment">Reproducibility</li>
  <ul>
  <li class="fragment">[GNU Make](https://www.gnu.org/software/make/)</li>
  </ul>
<li class="fragment">Controlled environment</li>
  <ul>
  <li class="fragment">[Docker](https://www.docker.com/) / [udocker](https://github.com/indigo-dc/udocker)</li>
  </ul>
</ul>

<a href="https://gitlab.com/StuntsPT/selectiondetection"><img src="assets/gitlab.png" class="fragment" alt="Gitlab logo" style="background:none; border:none; box-shadow:none; float: left;" /></a>

<div class="fragment" style="float: right"><img src="assets/Esteves.jpeg" alt="André Esteves" style="background:none; border:none; box-shadow:none;"/></div>

---

### Hands on

<a href="assets/selectiondetection.png"><img src="assets/selectiondetection.png" class="fragment" style="background:none; border:none; box-shadow:none;" alt="selection detection pipeline diagram" /></a>

---

### Parameter choice

<ul>
<li class="fragment">Standard "best practices"</li>
  <ul>
  <li class="fragment">`--snp` option in *Bayescan*</li>
  </ul>
<li class="fragment">Decision making</li>
  <ul>
  <li class="fragment">Choosing the value of "K" for LFMM</li>
    <ul>
    <li class="fragment">[Automated](https://gitlab.com/StuntsPT/pyRona/-/blob/master/pyRona/R/LFMM2_workflow.R#L58) ~~Looking at Deltas on a screeplot~~</li>
    </ul>
  </ul>
</ul>

<img src="assets/slade.png" class="fragment" alt="Parameter choice" style="float: center; background:none; border:none; box-shadow:none;" />

---

### So... what exactly is implemented?

<style>
.container{
display: flex;
}
.col{
flex: 1;
}
</style>
<div class="container">
<div class="col">
<div class="fragment">
Outlier detection
</br>
<ul>
<li class="fragment">Bayescan</li>
<li class="fragment">SelEstim</li>
<li class="fragment">PCAdapt</li>
<li class="fragment">OutFlank</li>
</ul>
</div>
</div>

<div class="col">
<div class="fragment">
Allele-variable associations
</br>
<ul>
<li class="fragment">CHELSA Bio vars</li>
<li class="fragment">Baypass</li>
<li class="fragment">LFMM</li>
<li class="fragment">LFMM2</li>
</ul>
</div>

</div>

---

### Data analyses challenges so far

<ul>
<li class="fragment">Run times</li>
  <ul>
  <li class="fragment">*SelEstim* takes ~80% of the CPU time</li>
  <li class="fragment">Largest datasets takes an estimated 25 days to run on a Ryzen 7 2700X CPU using all cores</li>
  </ul>
<li class="fragment">'Porting' to *udocker*</li>
  <ul>
  <li class="fragment">Can be run on [INCD's cluster](https://www.incd.pt/) resorting to a 320K CPU hours [project](https://www.fct.pt/apoios/Computacao/index.phtml.pt)</li>
  </ul>
</ul>

<img src="assets/incd.png" class="fragment" alt="INCD logo" style="float: center; background:none; border:none; box-shadow:none;" />

---

### Back to literature search...

<ul>
<li class="fragment" data-fragment-index="2">Search by:</li>
<div class="fragment" data-fragment-index="1" style="float: right"><img src="assets/raven_book.png" alt="TTG Raven reading a book about genome scans" style="background:none; border:none; box-shadow:none;"/></div>
  <ul>
  <li class="fragment" data-fragment-index="3">Keywords</li>
  <li class="fragment" data-fragment-index="4">Titles</li>
  <li class="fragment" data-fragment-index="5">Data type</li>
  </ul>
<li class="fragment" data-fragment-index="6">Title overview</li>
<li class="fragment" data-fragment-index="7">Repositories</li>
  <ul>
  <li class="fragment" data-fragment-index="8">Over 1000 candidate papers</li>
  </ul>
</ul>

|||

### ...and its hidden horrors!

<ul>
<div class="fragment" data-fragment-index="1" style="float: right"><img src="assets/raven_angry.png" alt="TTG Raven being angry" style="background:none; border:none; box-shadow:none;"/></div>
<li class="fragment" data-fragment-index="2">No SNP data</li>
<li class="fragment" data-fragment-index="3">No geographical data</li>
<li class="fragment" data-fragment-index="4">Non-matching samples</li>
<li class="fragment" data-fragment-index="5">Wrong/innacurate coordinates</li>
</ul>

---

### What's to show for it?

<p class="fragment">62 datasets</p>

<img src="assets/datasets.png" class="fragment" alt="datasets screenshot" style="float: center; background:none; border:none; box-shadow:none;" />

<a href="https://gitlab.com/StuntsPT/selection-detection-data/-/tree/master"><img src="assets/gitlab.png" class="fragment" alt="datasets screenshot" style="float: center; background:none; border:none; box-shadow:none;" />
</a>

|||

### What's to show for it?

<p class="fragment">Test suite</p>

<img src="assets/datasets_tests.png" class="fragment" alt="datasets screenshot" style="float: center; background:none; border:none; box-shadow:none;" />

<a href="https://gitlab.com/StuntsPT/selection-detection-data/-/jobs"><img src="assets/gitlab.png" class="fragment" alt="datasets screenshot" style="float: center; background:none; border:none; box-shadow:none;" />
</a>

---

### What's next?

<ul>
<li class="fragment" data-fragment-index="1" >MOAR DATA!</li>
  <ul>
  <li class="fragment" data-fragment-index="2" >Still plenty of papers to find</li>
  </ul>
<li class="fragment" data-fragment-index="3" >Marine environmental variable map data</li>
  <ul>
  <li class="fragment" data-fragment-index="4" >Should "unlock" an extra 12 curated datasets</li>
  </ul>
<li class="fragment" data-fragment-index="5" >Run everything!</li>
<li class="fragment" data-fragment-index="6" >Meta wrapper - to create meta-plots</li>
</ul>

<img src="assets/continued.png" class="fragment" alt="To be continued..." style="float: center; background:none; border:none; box-shadow:none;" />

---

### Acknowledgements

<img src="assets/Acknowledgements.png" style="background:none; border:none; box-shadow:none;" alt="Acknowledgements" style="text-align: center" />
